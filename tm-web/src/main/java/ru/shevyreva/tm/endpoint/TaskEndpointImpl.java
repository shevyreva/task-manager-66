package ru.shevyreva.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.shevyreva.tm.api.endpoint.ITaskEndpointImpl;
import ru.shevyreva.tm.model.Task;
import ru.shevyreva.tm.service.TaskService;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpointImpl implements ITaskEndpointImpl {
    @Autowired
    private TaskService taskService;

    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") String id) {
        return taskService.findById(id);
    }

    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        return taskService.save(task);
    }

    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        taskService.deleteById(id);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody Task task) {
        taskService.delete(task);
    }

}
