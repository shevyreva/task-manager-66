package ru.shevyreva.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.shevyreva.tm.enumerated.Status;
import ru.shevyreva.tm.model.Project;
import ru.shevyreva.tm.model.Task;
import ru.shevyreva.tm.service.ProjectService;
import ru.shevyreva.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {
    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;

    @GetMapping("/task/create")
    public String create() {
        taskService.save(new Task("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @Nullable final Task task = taskService.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Collection<Project> getProjects() {
        return projectService.findAll();
    }

    private Status[] getStatuses() {
        return Status.values();
    }

}
