package ru.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shevyreva.tm.api.repository.ITaskRepository;
import ru.shevyreva.tm.model.Task;

import java.util.List;

@Service
public class TaskService {
    @Autowired
    private ITaskRepository taskRepository;

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findById(@NotNull String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Transactional
    public Task save(@NotNull Task task) {
        return taskRepository.save(task);
    }

    public boolean existById(@NotNull String id) {
        return taskRepository.existsById(id);
    }

    public long count() {
        return taskRepository.count();
    }

    @Transactional
    public void deleteById(@NotNull String id) {
        taskRepository.deleteById(id);
    }

    @Transactional
    public void delete(@NotNull Task task) {
        taskRepository.delete(task);
    }

}
