package ru.t1.shevyreva.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.dto.IUserDtoService;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.exception.field.EmailEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.ExistsEmailException;
import ru.t1.shevyreva.tm.exception.user.ExistsLoginException;
import ru.t1.shevyreva.tm.exception.user.RoleEmptyException;
import ru.t1.shevyreva.tm.repository.dto.UserDtoRepository;
import ru.t1.shevyreva.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private UserDtoRepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);

        add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);
        user.setEmail(email);

        add(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(role);

        add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        return userRepository.findByEmail(email);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final UserDTO user = userRepository.findByLogin(login);
        userRepository.delete(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @NotNull final UserDTO user = userRepository.findByEmail(email);
        userRepository.delete(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final UserDTO user = userRepository.findOneById(id);
        userRepository.delete(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final UserDTO user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final UserDTO user = findOneById(id);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        userRepository.saveAndFlush(user);
        return user;
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        return (userRepository.findByLogin(login) != null);
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        return (userRepository.findByEmail(email) != null);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public UserDTO lockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull UserDTO user = findByLogin(login);
        user.setLocked(true);
        userRepository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public UserDTO unlockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull UserDTO user = findByLogin(login);
        user.setLocked(false);
        userRepository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final UserDTO user = userRepository.findOneById(id);
        if (user == null) throw new ModelNotFoundException();
        return user;
    }

    @SneakyThrows
    public List<UserDTO> findAll() {
        return userRepository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<UserDTO> add(@NotNull final Collection<UserDTO> models) {
        if (models == null) throw new UserNotFoundException();

        for (@NotNull UserDTO user : models) {
            add(user);
        }
        return models;
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        userRepository.deleteAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> models) {
        @Nullable final Collection<UserDTO> entities;
        removeAll();
        entities = add(models);
        return entities;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public UserDTO add(@NotNull final UserDTO model) {
        if (model == null) throw new UserNotFoundException();

        userRepository.saveAndFlush(model);
        return model;
    }

    @Override
    public @NotNull long getSize() {
        @Nullable final long count = userRepository.count();
        return count;
    }

}
