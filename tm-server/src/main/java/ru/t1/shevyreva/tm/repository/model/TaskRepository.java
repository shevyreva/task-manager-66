package ru.t1.shevyreva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends AbstractUserOwnedModelRepository<Task> {

    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Override
    @Transactional
    void deleteAll();

    @Override
    List<Task> findAll();

    @Override
    Optional<Task> findById(@NotNull final String id);

    List<Task> findByUserId(@NotNull final String userId);

    @Nullable
    @Query("SELECT p FROM Task p WHERE p.user = :userId ORDER BY :sortType")
    List<Task> findAllByUserIdWithSort(@NotNull String userId, @NotNull String sortType);


    Task findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Task findOneById(@NotNull final String id);

    @Override
    long count();

    long countByUserId(@NotNull final String userId);

    @Transactional
    void deleteOneByIdAndUserId(@NotNull final String userId, @NotNull final String id);

    @Transactional
    void deleteOneById(@NotNull String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
