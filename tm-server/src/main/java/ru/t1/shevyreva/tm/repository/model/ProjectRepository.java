package ru.t1.shevyreva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.model.Project;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends AbstractUserOwnedModelRepository<Project> {

    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Override
    @Transactional
    void deleteAll();

    @Override
    List<Project> findAll();

    @Override
    Optional<Project> findById(@NotNull final String id);

    List<Project> findByUserId(@NotNull final String userId);

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user = :userId ORDER BY :sortType")
    List<Project> findAllByUserIdWithSort(@NotNull String userId, @NotNull String sortType);

    Project findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Project findOneById(@NotNull final String id);

    @Override
    long count();

    long countByUserId(@NotNull final String userId);

    @Transactional
    void deleteOneByIdAndUserId(@NotNull final String id, @NotNull final String userId);

    @Transactional
    void deleteOneById(@NotNull String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
