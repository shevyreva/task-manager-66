package ru.t1.shevyreva.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
    }

    public AbstractSystemException(@Nullable final String message) {
        super(message);
    }

    public AbstractSystemException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
