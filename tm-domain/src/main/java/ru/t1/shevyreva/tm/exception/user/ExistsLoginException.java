package ru.t1.shevyreva.tm.exception.user;

public class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login has already existed.");
    }

}
