package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

@Service
public interface ILoggerService {

    void debug(@Nullable String message);

    void error(@Nullable Exception e);

    void info(@Nullable String message);

    void command(@Nullable String message);

}
