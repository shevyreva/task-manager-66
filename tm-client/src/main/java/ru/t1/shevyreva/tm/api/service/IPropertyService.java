package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.component.ISaltProvider;
import ru.t1.shevyreva.tm.api.endpoint.IConnectionProvider;

@Service
public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
