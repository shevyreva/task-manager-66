package ru.t1.shevyreva.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.dto.request.user.UserProfileRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private final String DESCRIPTION = "View user profile.";

    @NotNull
    private final String NAME = "user-profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull final UserDTO user = authEndpoint.profile(request).getUser();
        System.out.println("[USER PROFILE]");
        System.out.println("[USER ID]: " + user.getId());
        System.out.println("[LOGIN]: " + user.getLogin());
        System.out.println("[EMAIL]: " + user.getEmail());
        System.out.println("[FIRST NAME]: " + user.getFirstName());
        System.out.println("[LAST NAME]: " + user.getLastName());
        System.out.println("[MIDDLE NAME]: " + user.getMiddleName());
        System.out.println("[ROLE]: " + user.getRole());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
