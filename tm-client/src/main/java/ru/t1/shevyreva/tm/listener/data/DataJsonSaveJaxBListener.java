package ru.t1.shevyreva.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.data.DataSaveJsonJaxBRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class DataJsonSaveJaxBListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save Data to json file.";

    @NotNull
    private final String NAME = "data-save-json-jaxb";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataJsonSaveJaxBListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataSaveJsonJaxBRequest request = new DataSaveJsonJaxBRequest(getToken());
        domainEndpoint.saveJsonDataJaxB(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
