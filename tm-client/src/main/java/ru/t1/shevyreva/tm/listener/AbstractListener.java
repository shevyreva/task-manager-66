package ru.t1.shevyreva.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.IListener;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;


@Setter
@Getter
@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getName();

    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract void handler(@NotNull final ConsoleEvent consoleEvent);

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

}
